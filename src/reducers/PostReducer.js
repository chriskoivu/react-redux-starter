import { FETCH_POSTS, ADD_POST } from '../actions/actionTypes';

const initialState = {

  //items: [],
  posts:[],
  //item: {}
  post: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        posts: action.payload
      };
    case ADD_POST:
      return {
        ...state,
        post: action.payload
      };
    default:
      return state;
  }
}
